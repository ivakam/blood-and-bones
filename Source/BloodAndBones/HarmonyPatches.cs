using System;
using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using Verse;

namespace PorousBoat.BloodAndBones
{
    [HarmonyPatch(typeof(Pawn_EquipmentTracker), "Notify_EquipmentRemoved")]
    public class EquipmentTrackerPatches
    {
        static void Prefix(Pawn_EquipmentTracker __instance, ThingWithComps eq)
        {
            if (BloodAndBones.IsBoneWep(__instance.pawn.equipment.bondedWeapon))
            {
                eq.TryGetComp<CompBloodlinkWeapon>().Notify_EquipmentLost(__instance.pawn);
            }
        }
    }

    [HarmonyPatch(typeof(ReverseDesignatorDatabase), "InitDesignators")]
    public class DesignatorDatabasePatches
    {
        static private void Postfix(List<Designator> ___desList)
        {
            if (ModsConfig.IdeologyActive)
            {
                ___desList.Remove(___desList.Find(e => e.GetType() == typeof(Designator_ExtractSkull)));
                ___desList.Add(new Designator_ExtractBones());
            }
        }
    }
}