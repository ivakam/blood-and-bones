using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;

namespace PorousBoat.BloodAndBones
{
    public class RitualAttachableOutcomeEffectWorker_BloodlinkBestowal : RitualAttachableOutcomeEffectWorker
    {
        public override void Apply(Dictionary<Pawn, int> totalPresence, LordJob_Ritual jobRitual, OutcomeChance outcome, out string extraOutcomeDesc,
            ref LookTargets letterLookTargets)
        {
            List<Thing> boneWeps = GetBoneWeps(jobRitual);
            if (boneWeps.Count > 0)
            {
                Thing chosenWep = boneWeps.RandomElement();
                extraOutcomeDesc = def.letterInfoText.Formatted(chosenWep.Named("WEAPON"));
                chosenWep = BestowBloodlink(chosenWep, jobRitual);
            }
            else
            {
                extraOutcomeDesc = ("No suitable weapon found for bestowal");
            }
        }

        private List<Thing> GetBoneWeps(LordJob_Ritual r)
        {
            ThingRequest weps = ThingRequest.ForGroup(ThingRequestGroup.Weapon);
            List<Thing> boneWeps = r.Map.listerThings.ThingsMatching(weps);
            boneWeps = boneWeps.Where(w => BloodAndBones.IsBoneWep(w, true)).ToList();
            
            return boneWeps;
        }


        private Thing BestowBloodlink(Thing wep, LordJob_Ritual r)
        {
            // Terribly unsafe check for weapon type
            IntVec3 pos = wep.Position;
            wep.Destroy();
            switch (wep.def.defName)
            {
                case "BoneWeapon_Dagger":
                    return GenSpawn.Spawn(DefDatabase<ThingDef>.GetNamed("BoneWeapon_DaggerBloodlink"), pos, r.Map);
                case "BoneWeapon_Sword":
                    return GenSpawn.Spawn(DefDatabase<ThingDef>.GetNamed("BoneWeapon_SwordBloodlink"), pos, r.Map);
                case "BoneWeapon_Club":
                    return GenSpawn.Spawn(DefDatabase<ThingDef>.GetNamed("BoneWeapon_ClubBloodlink"), pos, r.Map);
                default:
                    return GenSpawn.Spawn(DefDatabase<ThingDef>.GetNamed("BoneWeapon_DaggerBloodlink"), pos, r.Map);
            }
        }
    }
}