using System;
using System.Collections.Generic;
using System.Linq;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;

namespace PorousBoat.BloodAndBones
{
    public class JobDriver_ExtractBones : JobDriver_ExtractSkull
    {
        protected override IEnumerable<Toil> MakeNewToils()
        {
            if (!ModLister.CheckIdeology("Skull extraction"))
            {
                yield break;
            }
            this.FailOn(() => Corpse.Destroyed || !Corpse.Spawned);
            yield return Toils_Reserve.Reserve(TargetIndex.A);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.OnCell);
            Toil toil = Toils_General.Wait(180);
            toil.WithProgressBarToilDelay(TargetIndex.A);
            toil.PlaySustainerOrSound(SoundDefOf.Recipe_Surgery);
            toil.tickAction = (Action)Delegate.Combine(toil.tickAction, (Action)delegate
            {
                if (Rand.Chance(0.016666668f))
                {
                    IntVec3 randomCell = new CellRect(Corpse.PositionHeld.x - 1, Corpse.PositionHeld.z - 1, 3, 3).RandomCell;
                    if (randomCell.InBounds(base.Map) && GenSight.LineOfSight(randomCell, Corpse.PositionHeld, base.Map))
                    {
                        FilthMaker.TryMakeFilth(randomCell, Corpse.MapHeld, Corpse.InnerPawn.RaceProps.BloodDef, pawn.LabelIndefinite());
                    }
                }
            });
            yield return toil;
            yield return Toils_General.Do(delegate
            {
                DesignationDef extractBones = DefDatabase<DesignationDef>.GetNamed("ExtractBones");
                //Remove skull
                Corpse.InnerPawn.health.AddHediff(HediffDefOf.MissingBodyPart, Corpse.InnerPawn.health.hediffSet.GetNotMissingParts().First((BodyPartRecord p) => p.def == BodyPartDefOf.Head));
                Corpse.Map.designationManager.RemoveDesignation(Corpse.Map.designationManager.DesignationOn(Corpse, extractBones));
                GenPlace.TryPlaceThing(ThingMaker.MakeThing(ThingDefOf.Skull), Corpse.PositionHeld, Corpse.Map, ThingPlaceMode.Near);

                //Roll twice (one for each arm and leg)
                for (int i = 0; i < 2; i++)
                {
                    if (Rand.Chance(0.7f) && Corpse.InnerPawn.health.hediffSet.GetNotMissingParts().Any(p => p.def == BodyPartDefOf.Leg))
                    {
                        //Remove femur
                        Corpse.InnerPawn.health.AddHediff(HediffDefOf.MissingBodyPart, Corpse.InnerPawn.health.hediffSet.GetNotMissingParts().First((BodyPartRecord p) => p.def == BodyPartDefOf.Leg));
                        GenPlace.TryPlaceThing(ThingMaker.MakeThing(ThingDef.Named("Femur")), Corpse.PositionHeld, Corpse.Map, ThingPlaceMode.Near);
                    }
                    if (Rand.Chance(0.7f) && Corpse.InnerPawn.health.hediffSet.GetNotMissingParts().Any(p => p.def == BodyPartDefOf.Arm))
                    {
                        //Remove humerus
                        Corpse.InnerPawn.health.AddHediff(HediffDefOf.MissingBodyPart, Corpse.InnerPawn.health.hediffSet.GetNotMissingParts().First((BodyPartRecord p) => p.def == BodyPartDefOf.Arm));
                        GenPlace.TryPlaceThing(ThingMaker.MakeThing(ThingDef.Named("Humerus")), Corpse.PositionHeld, Corpse.Map, ThingPlaceMode.Near);
                    }
                }
            });
        }
    }

    public class Designator_ExtractBones : Designator_ExtractSkull
    {
        protected DesignationDef extractBones = DefDatabase<DesignationDef>.GetNamed("ExtractBones");
        
        public Designator_ExtractBones()
        {
            if (ModLister.CheckIdeology("Skull extraction"))
            {
                defaultLabel = "DesignatorExtractBones".Translate();
                defaultDesc = "DesignatorExtractBonesDesc".Translate();
                icon = ContentFinder<Texture2D>.Get("UI/Designators/ExtractSkull");
                useMouseIcon = true;
                soundDragSustain = SoundDefOf.Designate_DragStandard;
                soundDragChanged = SoundDefOf.Designate_DragStandard_Changed;
                soundSucceeded = SoundDefOf.Designate_ExtractSkull;
                hotKey = KeyBindingDefOf.Misc3;
                tutorTag = "ExtractSkull";
            }
        }

        public override void DesignateThing(Thing t)
        {
            Map.designationManager.AddDesignation(new Designation(t, extractBones));
        }
    }

    public class WorkGiver_ExtractBones : WorkGiver_ExtractSkull
    {
        protected DesignationDef extractBones = DefDatabase<DesignationDef>.GetNamed("ExtractBones");
        
        public override IEnumerable<Thing> PotentialWorkThingsGlobal(Pawn pawn)
        {
            foreach (Designation item in pawn.Map.designationManager.SpawnedDesignationsOfDef(extractBones))
            {
                yield return item.target.Thing;
            }
        }

        public override bool ShouldSkip(Pawn pawn, bool forced = false)
        {
            return !pawn.Map.designationManager.AnySpawnedDesignationOfDef(extractBones);
        }

        public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            if (!ModLister.CheckIdeology("Skull extraction"))
            {
                return false;
            }
            Corpse corpse = t as Corpse;
            if (corpse == null || corpse.Destroyed)
            {
                return false;
            }
            if (corpse.Map.designationManager.DesignationOn(t, extractBones) == null)
            {
                return false;
            }
            if (!corpse.InnerPawn.health.hediffSet.HasHead)
            {
                return false;
            }
            if (!pawn.CanReserve(t, 1, -1, null, forced))
            {
                return false;
            }
            if (pawn.Ideo == null || !CanExtractSkull(pawn.Ideo))
            {
                JobFailReason.Is("CannotExtractSkull".Translate());
                return false;
            }
            return true;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            Job job = JobMaker.MakeJob(DefDatabase<JobDef>.GetNamed("ExtractBones"), t);
            job.count = 1;
            return job;
        } 
    }
}