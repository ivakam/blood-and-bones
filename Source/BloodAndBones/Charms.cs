using System;
using System.Reflection;
using Verse;
using RimWorld;
using UnityEngine;
using Verse.Sound;
using Vector3 = UnityEngine.Vector3;
using Matrix4x4 = UnityEngine.Matrix4x4;
using Quaternion = UnityEngine.Quaternion;

namespace PorousBoat.BloodAndBones
{
    public class Verb_ShadowStep : Verb_Jump
    {
        //TODO: Move to xml
        private float severityIncrement = 0.2f;

        public override bool CasterIsPawn => caster is Pawn && !CasterPawn.Dead;
        
        protected override bool TryCastShot()
        {
	        Log.Message("Trying shadowstep...");
            if (!ModLister.CheckRoyalty("Jumping"))
            {
                return false;
            }

            Pawn casterPawn = CasterPawn;
            if (casterPawn == null)
            {
                return false;
            }

            IntVec3 cell = currentTarget.Cell;
            Map map = casterPawn.Map;
			ApplyJumpDamage();

			if (casterPawn.Dead)
			{
				return false;
			}
            PawnFlyer pawnFlyer = PawnFlyer.MakeFlyer(ThingDef.Named("PawnShadowStepper"), casterPawn, cell);
            if (pawnFlyer != null)
            {
                GenSpawn.Spawn(pawnFlyer, cell, map);
				return true;
            }

            return false;
        }

        protected void ApplyJumpDamage()
        {
            Pawn p = CasterPawn;
            
            if (p.health.hediffSet.HasHediff(HediffDef.Named("ShadowStepSickness")))
            {
                p.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("ShadowStepSickness")).Severity +=
                    severityIncrement;
                p.health.Notify_HediffChanged(p.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("ShadowStepSickness")));
            }
            else
            {
                p.health.AddHediff(HediffDef.Named("ShadowStepSickness"));
            }
        }
    }

    public class PawnShadowStepper : PawnFlyer
    {
		private static readonly Func<float, float> FlightSpeed;
		private static readonly Func<float, float> FlightCurveHeight;
		private Material cachedShadowMaterial;
		private Effecter flightEffecter;
		private int positionLastComputedTick = -1;
		private Vector3 groundPos;
		private Vector3 effectivePos;
		private float effectiveHeight;

		private Material ShadowMaterial
		{
			get
			{
				if (cachedShadowMaterial == null && !def.pawnFlyer.shadow.NullOrEmpty())
				{
					cachedShadowMaterial = MaterialPool.MatFrom(def.pawnFlyer.shadow, ShaderDatabase.Transparent);
				}
				return cachedShadowMaterial;
			}
		}

		public override Vector3 DrawPos
		{
			get
			{
				RecomputePosition();
				return effectivePos;
			}
		}

		static PawnShadowStepper()
		{
			FlightCurveHeight = GenMath.InverseParabola;
			AnimationCurve animationCurve = new AnimationCurve();
			animationCurve.AddKey(0f, 0f);
			animationCurve.AddKey(0.1f, 0.15f);
			animationCurve.AddKey(1f, 1f);
			FlightSpeed = animationCurve.Evaluate;
		}

		protected override bool ValidateFlyer()
		{
			if (!ModLister.CheckRoyalty("Jumping"))
			{
				return false;
			}
			return true;
		}

		private void RecomputePosition()
		{
			if (positionLastComputedTick != ticksFlying)
			{
				positionLastComputedTick = ticksFlying;
				float arg = (float)ticksFlying / (float)ticksFlightTime;
				float num = FlightSpeed(arg);
				effectiveHeight = FlightCurveHeight(num);
				groundPos = Vector3.Lerp(startVec, base.DestinationPos, num);
				Vector3 vector = new Vector3(0f, 0f, 2f);
				Vector3 vector2 = Altitudes.AltIncVect * effectiveHeight;
				Vector3 vector3 = vector * effectiveHeight;
				//effectivePos = groundPos + vector2 + vector3;
				effectivePos = groundPos;
			}
		}

		public override void DrawAt(Vector3 drawLoc, bool flip = false)
		{
			RecomputePosition();
			DrawShadow(groundPos, effectiveHeight);
			base.FlyingPawn.DrawAt(effectivePos, flip);
		}

		private void DrawShadow(Vector3 drawLoc, float height)
		{
			Material shadowMaterial = ShadowMaterial;
			if (!(shadowMaterial == null))
			{
				float num = Mathf.Lerp(1f, 0.6f, height);
				Vector3 s = new Vector3(num, 1f, num);
				Matrix4x4 matrix = default(Matrix4x4);
				matrix.SetTRS(drawLoc, Quaternion.identity, s);
				Graphics.DrawMesh(MeshPool.plane10, matrix, shadowMaterial, 0);
			}
		}

		protected override void RespawnPawn()
		{
			LandingEffects();
			base.RespawnPawn();
		}

		public override void Tick()
		{
			if (flightEffecter == null && def.pawnFlyer.flightEffecterDef != null)
			{
				flightEffecter = def.pawnFlyer.flightEffecterDef.Spawn();
				flightEffecter.Trigger(this, TargetInfo.Invalid);
			}
			else
			{
				flightEffecter?.EffectTick(this, TargetInfo.Invalid);
			}
			base.Tick();
		}

		private void LandingEffects()
		{
			if (def.pawnFlyer.soundLanding != null)
			{
				def.pawnFlyer.soundLanding.PlayOneShot(new TargetInfo(base.Position, base.Map));
			}
			FleckMaker.ThrowDustPuff(base.DestinationPos + Gen.RandomHorizontalVector(0.5f), base.Map, 2f);
		}

		public override void Destroy(DestroyMode mode = DestroyMode.Vanish)
		{
			flightEffecter?.Cleanup();
			base.Destroy(mode);
		}
    }

	[StaticConstructorOnStartup]
    public class CharmOfWarding : ShieldBelt
    {
	    public CharmOfWarding()
	    {
		    Material m = MaterialPool.MatFrom("CharmBubble", ShaderDatabase.Transparent);
			var field = typeof(ShieldBelt).GetField("BubbleMat", BindingFlags.Static | BindingFlags.NonPublic);
			Log.Message("Old tex path: " + field.GetValue(this));
			field?.SetValue(this, m);
			Log.Message("New tex path: " + field.GetValue(this));
	    }
    }
}