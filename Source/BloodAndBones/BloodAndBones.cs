﻿using System;
using System.Text.RegularExpressions;
using HarmonyLib;
using RimWorld;
using Verse;

namespace PorousBoat.BloodAndBones
{
    [StaticConstructorOnStartup]
    public class BloodAndBones
    {
        static BloodAndBones()
        {
            Log.Message("BLOOD & BONES: Running...");
            harmonyInstance = new Harmony("PorousBoat.BloodAndBones");
            harmonyInstance.PatchAll();
        }

        public static Harmony harmonyInstance;
        
        public static bool IsBoneWep(Thing t, bool exclude_bloodlink = false)
        {
            if (t == null)
            {
                return false;
            }
            String pattern = @"BoneWeapon.+";
            if (exclude_bloodlink)
            {
                // A little janky, fix sometime probably
                pattern = @"^BoneWeapon_(Sword|Dagger|Club)$";
            }
            Regex r = new Regex(pattern);
            return r.IsMatch(t.def.defName);
        }
    }

    public class CompProperties_BloodlinkWeapon : CompProperties_Biocodable
    {
        public CompProperties_BloodlinkWeapon()
        {
            compClass = typeof(CompBloodlinkWeapon);
        }
    }
}