using System;
using System.Collections.Generic;
using System.Reflection;
using RimWorld;
using Verse;

namespace PorousBoat.BloodAndBones
{
	public class WeaponTraitDef_Bone : WeaponTraitDef
	{
		protected WeaponTraitWorker_Bone worker_bone;

		public WeaponTraitDef_Bone(WeaponTraitDef t)
		{
			var boneType = this.GetType();
			var vanillaProps = t.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);

			// EXTREMELY BIG HACK WARNING THIS IS TERRIBLE
			foreach (var p in vanillaProps)
			{
				boneType.GetField(p.Name).SetValue(this, p.GetValue(t));
			}
		}
		public WeaponTraitWorker_Bone Worker_Bone
		{
			get
			{
				if (worker_bone == null)
				{
					worker_bone = (WeaponTraitWorker_Bone) Activator.CreateInstance(workerClass);
					worker_bone.def = this;
				}

				return worker_bone;
			}
		}
	}

	public class WeaponTraitWorker_Bone : WeaponTraitWorker
	{
		public void Notify_OtherWeaponWielded(CompBloodlinkWeapon weapon)
		{
			
		}
	}

	public class WeaponTraitWorker_BloodThirst : WeaponTraitWorker_Bone
	{
		public float dmgCoeff = 1.0f;

		public void resetDmgCoeff(Pawn pawn)
		{
			dmgCoeff = 1.0f;
			applyDmgCoeff(pawn);
		}
		
		//FIXME: Applies coeff to all Things sharing the CompEquippable
		public void applyDmgCoeff(Pawn pawn)
		{
			CompEquippable wep = pawn.equipment.bondedWeapon.TryGetComp<CompEquippable>();
			List<Tool> startPowers = pawn.equipment.bondedWeapon.def.tools;
			for (int i = 0; i < wep?.Tools.Count; i++)
			{
				wep.Tools[i].power = startPowers[i].power * dmgCoeff;
			}
		}
		
		public override void Notify_KilledPawn(Pawn pawn)
		{
			base.Notify_KilledPawn(pawn);
			dmgCoeff = Math.Max(dmgCoeff + 0.05f, 1.2f);

			//Add null check maybe?
			Hediff wrath = pawn.health.hediffSet.hediffs.Find(h => h.def.defName == "BloodThirst");
			wrath.Severity = 0.01f;
			
			applyDmgCoeff(pawn);
		}
	}

	public class WeaponTraitWorker_ShadowDance : WeaponTraitWorker_Bone
	{
		public override void Notify_Equipped(Pawn pawn)
		{
			base.Notify_Equipped(pawn);
			
			TraitDef td = DefDatabase<TraitDef>.GetNamed("Nimble");
			if (!pawn.story.traits.HasTrait(td))
			{
				Trait t = new Trait(td);
				pawn.story.traits.GainTrait(t);
			}
		}

		public override void Notify_EquipmentLost(Pawn pawn)
		{
			base.Notify_EquipmentLost(pawn);
			
			TraitDef td = DefDatabase<TraitDef>.GetNamed("Nimble");
			if (pawn.story.traits.HasTrait(td))
			{
				Trait t = pawn.story.traits.GetTrait(td);
				pawn.story.traits.RemoveTrait(t);
			}
		}
	}
	
	public class WeaponTraitWorker_UnholyCuts : WeaponTraitWorker_Bone
	{
		public override void Notify_Bonded(Pawn pawn)
		{
			base.Notify_Bonded(pawn);
			CompEquippable wep = pawn.equipment.bondedWeapon.TryGetComp<CompEquippable>();
			wep?.Tools.ForEach(t => t.armorPenetration *= 1.2f);
		}

		public override void Notify_Unbonded(Pawn pawn)
		{
			base.Notify_Unbonded(pawn);
			CompEquippable wep = pawn.equipment.bondedWeapon.TryGetComp<CompEquippable>();
			wep?.Tools.ForEach(t => t.armorPenetration /= 1.2f);
		}

		public override void Notify_EquipmentLost(Pawn pawn)
		{
			base.Notify_EquipmentLost(pawn);

			Hediff shadowDance = pawn.health.hediffSet.hediffs.Find(h => h.def.defName == "UnholyCuts");
			if (shadowDance != null)
			{
				pawn.health.RemoveHediff(shadowDance);
			}
		}
	}

	public class WeaponTraitWorker_BrutalStrikes : WeaponTraitWorker_Bone
	{
		
	}
}