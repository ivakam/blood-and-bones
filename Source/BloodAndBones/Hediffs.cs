using Verse;

namespace PorousBoat.BloodAndBones
{
    public class Hediff_ShadowStepSickness : HediffWithComps
    {
		private float intervalFactor;

		public override void PostMake()
		{
			base.PostMake();
			intervalFactor = Rand.Range(0.1f, 2f);
		}

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref intervalFactor, "intervalFactor", 0f);
		}

		public override void Tick()
		{
			base.Tick();
			if (pawn.IsHashIntervalTick((int)(5000f * intervalFactor)))
			{
				Severity += Rand.Range(-0.03f, -0.05f);
			}
		}
    }
    
	public class Hediff_BloodThirst : HediffWithComps
	{
		private float intervalFactor;

		public override void PostMake()
		{
			base.PostMake();
			intervalFactor = Rand.Range(0.1f, 2f);
		}

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref intervalFactor, "intervalFactor", 0f);
		}

		public override void Tick()
		{
			base.Tick();
			if (pawn.IsHashIntervalTick((int)(5000f * intervalFactor)))
			{
				Severity += Rand.Range(0.03f, 0.05f);
				if (SeverityLabel == "unhappy")
				{
					pawn.equipment.bondedWeapon.TryGetComp<CompBloodlinkWeapon>().ResetBloodThirst();
				}
			}
		}
	}
}